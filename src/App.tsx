import React from "react"

import { connect, useSelector, useDispatch } from "react-redux"

import { ipcRenderer } from "electron"

import { RootState } from "./store/root-reducer"
import { increment } from "./store/actions"

import "./App.css";

const sendCount = (c: number) => {
  ipcRenderer.send("sendCount", c)
}
const App: React.FC = () => {
  const counter = useSelector((state: RootState) => state.counter)
  const dispatch = useDispatch()
  return (
    <div className="App">
      <header className="App-header">
        <p>
          {counter}
        </p>
        <button onClick={_ => dispatch(increment())}>Click Me!</button>
        <button onClick={_ => sendCount(counter)}>Write Count To File</button>
      </header>
    </div>
  );
}

export default connect()(App);
