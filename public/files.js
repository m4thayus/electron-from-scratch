const fs = require("fs-extra")
const ipc = require("electron").ipcMain

const file = "tmp/count.txt"

ipc.on("sendCount", (_event, resp) => {
  writeCount(file, resp)
})

async function writeCount (f, c) {
  try {
    await fs.outputFile(f, c)

    const data = await fs.readFile(f, "utf8")

    console.log(data)
  } catch (err) {
    console.log(err)
  }
}
