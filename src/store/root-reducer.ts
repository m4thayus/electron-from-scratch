import { ActionType, createReducer } from "typesafe-actions"

import * as actions from "./actions"

export type RootAction = ActionType<typeof actions>

export type RootState = {
  counter: number
}

const initialState: RootState = {
  counter: 0
}

export default createReducer(initialState, {
  INCREMENT: (state) => ({ ...state, counter: state.counter + 1 })
})
