import { action } from "typesafe-actions"

export const increment = () => action("INCREMENT")
